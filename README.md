# BLUEBERRY ANSIBLE

## Description
Ce projet est un _playbook ansible_ qui permet de déployer une sonde NIDS avec le logiciel de management [Blueberry](https://gitlab.com/mic-rigaud/blueberry) sur une Raspberry ou une VM Debian.
Le projet s'attache également à durcir l'OS en suivant le [Guide ANSSI](https://www.ssi.gouv.fr/uploads/2016/01/linux_configuration-fr-v1.2.pdf), et les recommandations [Lynis](https://cisofy.com/lynis/).

## Ce que fait ce projet
Il permet de déployer et configurer les outils suivants sur Raspbian ou Debian :
- durcissement de l'OS en suivant le [Guide ANSSI](https://www.ssi.gouv.fr/uploads/2016/01/linux_configuration-fr-v1.2.pdf), et les recommandations [Lynis](https://cisofy.com/lynis/)
- apparmor
- ufw
- sshd
- fail2ban
- logwatch
- suricata
- arpwatch
- chkrootkit
- [blueberry](https://gitlab.com/mic-rigaud/blueberry), outil de monitoring et administration maison à travers un [bot telegram](https://core.telegram.org/bots/)

## Ce que ne fait pas ce projet
- Enlever l'utilisateur par défaut `pi`
- Ajouter un utilisateur
- Changer le mot de passe utilisateur

## Prérequis
- Dupliquer `./exemple` en `./production`. `cp -r exemple production`
- Remplir production/hosts
- Remplir production/group_vars/all.yaml
- Posséder un utilisateur qui a les droits sudo.
    - Vérifier que `sudo` est installé. `apt install sudo`
    - Vérifier que l'utilisateur est dans le group sudo. `adduser {user} sudo`
- Python-apt est installé. `apt install python-apt`

## Utilisation
```shell
ansible-playbook -i production --ask-become-pass main.yml --ask-pass
```

**Attention: pendant l’exécution du script, le port de connexion ssh va être changé. Il faut donc penser à renseigner le nouveau port dans production/hosts si vous souhaitez dérouler une seconde fois le projet.**

## Questions
### Est-ce que je peux utiliser ce projet sans installer blueberry?
Oui. Il suffit de commenter la ligne suivante dans `main.yml`:

`- import_playbook: blueberry.yml`

**Attention: pensez à mettre un autre outil de supervision sinon vous ne serez jamais informé des alarmes de sécurité.**

### Pourquoi ne pas utiliser les mails pour monitorer?
Parce que je n'aime pas ça. En général ça pollue les boites mails, cela ne fait que renvoyer les erreurs et ne permet pas de faire des actions.


## BUGS connus
### SSHD
Il arrive que sshd ne redémarre pas après le script. Mais ufw oui. Attention donc a ne pas finir bloqué après l'installation.

Conseil: avant de lancer le script d'installation, connecté vous en ssh à votre équipement.
Puis lancer le script sans se déconnecter. Une fois fini, lancer ```systemctl restart sshd```

### Suricata
Il arrive que le script aille trop vite pour suricata et que donc le démarrage du service ne fonctionne pas. Si cela arrive, relancer le script.
